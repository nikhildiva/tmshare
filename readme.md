# TMSHARE
This is a tweaking of tmux. So you will need tmux installed in your system. This program will help you share your session across users and have control over the same.


## CAUTION!!

If you are opening a tmshare session and sharing with someone, always be sure to kill the session after use. Also share your sessions with persons you trust only.

## HOW TO INSTALL?

- Clone repository
- Go to cloned folder, and run
``` 
sh install.sh
```

Run tmshare -h to see the help and verify installation

As easy as that!!

## HOW TO USE?

Run tmshare -h after installation. You can see all options and what it does

## TMUX SHORTCUTS

This package comes with a tmux config with my favourite features and shortcuts. Mentioning very handy ones here.

Inside tmux session,

Alt-n		-	New window

Alt-r		-	Rename window

Shift-right	-	Next window

Shift-left	-	Previous window

Shift-up	-	Swap window to left

Shift-down	-	Swap window to right

Alt + arrow -	Move to specific pane

Ctrl-b d	-	Detach session (This shortcut comes with tmux. Still adding here for reference)

Ctrl-b "	- 	Split horizonatlly

Ctrl-b %	- 	Split vertically

Ctrl-b z	- 	toggle fullscreen

NB: Tmux shortcuts are case sensitive. So before frantically hitting the keys, take a look at Caps lock status if a shortcut is not working.
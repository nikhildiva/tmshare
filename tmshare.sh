#!/bin/bash

# options
# set -x

#common constants
socket="/tmp/nowucme"
session_prefix='owner-'
session=$session_prefix$USER
session_unavail=""

## creating an alias function 
tm(){ tmux -S $socket/$USER "$@"; }

## creating an alias function for joining another session
tmj(){ tmux -S $socket/"$@"; }

# to display help message

usage(){
	printf "usage: tmshare [-hkl][-j user][-u [user|user1,user2,user3]]\n" 1>&2
	printf "This is script is built on top of tmux. To start your session, just run tmshare. Other options are as follows\n" 1>&2
	printf "\t-d\tKicks out and blocks all users from the session. Tmux can't kick out a specific user from a session. So this command is bad but only work around if undesirable should happen\n" 1>&2
	printf "\t-h\tLoads this message\n" 1>&2
	printf "\t-k\tKills your session\n" 1>&2
	printf "\t-l\tLists users with access to your session\n" 1>&2
	printf "\t-j user\n\t\tTo join another user's session\n"
	printf "\t-u user\n\t-u user1,user2,user3\n" 1>&2
	printf "\t\tToggles access to user(s) to your session. If you remove access to a user, it doesn't ensure person is kicked out unless you run tmshare -d\n" 1>&2
}


# to kick all users from your session

kick_all(){

	setfacl -b $socket/$USER
	chmod 200 $socket/$USER
	tm detach-client -s $session

}


# starts a session socket and set access permission for only the owner 

start_session(){
	mkdir -p $socket
	session_exists
	if [ $? -eq 0 ] ; then
		tm new -d -s $session
		chmod 777 $socket
		chmod 200 $socket/$USER
	fi
	tm attach -t $session
}


# verifies whether session for the user exists already and return 1 if found and 0 otherwise
session_exists(){
	
	grep_string=$session

# will verify existance of another session if parameter passed

	if [ $# -ne 0 ]; then
		grep_string=$session_prefix$1
		
		tmj $1 ls &> /dev/null
		if [ $? -eq 0 ]; then
			! tmj $1 ls | grep -E ^$grep_string:>>/dev/null
			return $? 
		else
			return 0
		fi
	fi

# if ls fails, we can safely assume session is absent. If not, grep the output with session name
	tm ls &> /dev/null
	if [ $? -eq 0 ]; then
		! tm ls | grep -E ^$grep_string:>>/dev/null
		return $? 
	else
		return 0
	fi

}

# to join another user's session if access granted. usage: join_session hostusername

join_session(){
	
	host_session=$session_prefix$1
	session_exists $1
	if [ $? -eq 1 ]; then
		tmj $1 attach -t $host_session
	else
		echo "No session for user $1 or you have no access"
	fi
}

#ummm..you know

kill_session(){
	tm kill-session -t $session
}

#to toggle guest user access. Usage has_access username

set_permission(){
	has_access $1
	
	if [ $? -eq 1 ]; then
		remove_user $1
	else
		add_user $1
	fi	 
}

#to verify whether a user has access. usage: has_access username

has_access(){
	str=$( getfacl -e $socket/$USER 2>&1|grep -E "^user:$1:" )
	perm=${str: -3}
	if [[ "$perm" =~ [r-]w[x-] ]]; then
		return 1
	else 
		return 0
	fi
	
}

add_user(){
	setfacl -m u:$1:-w- $socket/$USER &> /dev/null

	if [ $? -eq 0 ]; then
		echo "Access granted to user $1"
	else
		echo "No such user - $1"
	fi
}

remove_user(){
	
	if [ $? -eq 0 ]; then
		if [ $1 != $USER ]; then
			setfacl -m u:$1:--- $socket/$USER &> /dev/null
			echo "Access revoked for user $1 to new connections. Run tmshare -d to ensure user is kicked off from current sessions."
		else
			echo "You can't kick yourself!! No action taken for $USER"
		fi
	else
		echo "No such user - $1"
	fi
}

show_users(){
	
	users=$( getfacl -e $socket/$USER 2>&1 | grep -E "#effective:[r-]w[x-]"|grep -o :[a-zA-Z]*:|tr -d : )
	
	if [ ${#users} -ne 0 ]; then
		echo "Following user(s) can access your session:"
		echo $users
	else
		echo "You have no guests"
	fi
}


while getopts "dj:klht:u:" arg; do

	case $arg in

		d)
			kick_all;;
		j)
			join_session $OPTARG;;
		h)
			usage;;
		k)
			kill_session;;	
		l)
			show_users;;
		u)
			set -f
			IFS=, 
			array=($OPTARG)
			for i in ${array[@]}
			do
				set_permission $i
			done;;
		*)
			usage
			exit 1;;

	esac

done

if [ $# -eq 0 ]; then
	start_session
fi

